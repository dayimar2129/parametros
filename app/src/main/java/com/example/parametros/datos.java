package com.example.parametros;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class datos extends AppCompatActivity {

    TextView nombre_,apellido_,telefono_,ciudad_;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);

        nombre_ = (TextView) findViewById(R.id.nombre);
        apellido_ = (TextView)findViewById(R.id.apellido);
        telefono_ = (TextView)findViewById(R.id.telefono);
        ciudad_ = (TextView) findViewById(R.id.ciudad);

        Bundle bundle = this.getIntent().getExtras();
        nombre_.setText(bundle.getString("nombre"));
        apellido_.setText(bundle.getString("apellido"));
        telefono_.setText(bundle.getString("telefono"));
        ciudad_.setText(bundle.getString("ciudad"));
    }
}
