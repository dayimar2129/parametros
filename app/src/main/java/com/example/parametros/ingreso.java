package com.example.parametros;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EdgeEffect;
import android.widget.EditText;

public class ingreso extends AppCompatActivity {


    EditText nombre_1, apellido_, telefono_, ciudad_;

    Button env_datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso);


        nombre_1 = (EditText) findViewById(R.id.nombre);
        apellido_ = (EditText) findViewById(R.id.apellido);
        telefono_ = (EditText) findViewById(R.id.telefono);
        ciudad_ = (EditText)findViewById(R.id.ciudad);

        env_datos = (Button) findViewById(R.id.env_datos);

        env_datos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ingreso.this, datos.class);

                Bundle nombre_2 = new Bundle();
                nombre_2.putString("nombre", nombre_1.getText().toString());
                i.putExtras(nombre_2);

                Bundle apellido = new Bundle();
                apellido.putString("apellido", apellido_.getText().toString());
                i.putExtras(apellido);

                Bundle telefono = new Bundle();
                telefono.putString("telefono", telefono_.getText().toString());
                i.putExtras(telefono);

                Bundle ciudad = new Bundle();
                ciudad.putString("ciudad", ciudad_.getText().toString());
                i.putExtras(ciudad);

                startActivity(i);
            }
        });
    }
}
